//
//  EsferiatUITests.swift
//  EsferiatUITests
//
//  Created by Mohammed Salah on 30/08/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//

import XCTest

class EsferiatUITests: XCTestCase {

    

    func testExample() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        let masterNav = app.navigationBars["Master"]
        setupSnapshot(app)
        app.launch()
        
        snapshot("Login")
        
        let password = app.textFields["password"]
        let userName = app.textFields["userName"]
        
        userName.tap()
        userName.typeText("test")

        password.tap()
        password.typeText("test")
        
        if app.keyboards.element(boundBy: 0).exists {
            if UIDevice.current.userInterfaceIdiom == .pad {
                app.keyboards.buttons["Hide keyboard"].tap()
            } else {
                app.toolbars.buttons["Done"].tap()
            }
        }
        
        app.buttons["loginBtn"].tap()
        snapshot("Home")
        app.buttons["ChangePass"].tap()
        snapshot("ChangePassSnap")
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

}

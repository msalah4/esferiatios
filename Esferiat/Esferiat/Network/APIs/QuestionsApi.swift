//
//  QuestionsApi.swift
//  Esferiat
//
//  Created by Mohammed Salah on 19/08/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//

import Alamofire
import PromiseKit
import RxSwift


class QuestionsApi: Api {
    
    var Sub_ID = ""
    var answer: String = ""
    var qID = ""
    var email = ""
    var password = ""
    var supportType = ""
    var noteType = ""
    var phone = ""
    
    init(params: RequestParamters?) {
        super.init()
        self.params = params
    }
    
    enum APIRouter: Requestable {
        case getUserInfo(QuestionsApi)
        case getMonthQuestion(QuestionsApi)
        case submitMonthQuestionAnswer(QuestionsApi)
        case updateEmail(QuestionsApi)
        case updatePassword(QuestionsApi)
        case getConfirmationCode(QuestionsApi)
        case deleteAccount(QuestionsApi)
        case fetchCards
        case showSupport(QuestionsApi)
        case downloadPDF
        case getPartners
        case getMonthQuestionOptions(QuestionsApi)
        
        var path: String {
            switch self {
            case .getUserInfo:
                return "ESNew/api/EsferiatServMainData"
            case .getMonthQuestion:
                return "ESNew/api/EsferiatServGetQuestion"
            case .submitMonthQuestionAnswer:
                return "ESNew/api/EsferiatServAnswer"
            case .updateEmail:
                return "ESNew/api/EsferiatServUpdateEmail"
            case .updatePassword:
                return "ESNew/api/EsferiatServUpdatePassword"
            case .getConfirmationCode:
                return "ESNew/api/EsferiatServLoginAuth"
            case .deleteAccount:
                return "/ESNew/api/EsferiatServDelete"
            case .fetchCards:
                return "/ESNew/api/EsferiatServGetCards"
            case .showSupport:
                return "/ESNew/api/EsferiatServSupportRequest"
            case .downloadPDF:
                return "/ESNew/api/EsferiatServGetPDF"
            case .getPartners:
                return "/ESNew/api/EsferiatServGetParteners"
            case .getMonthQuestionOptions:
                return "/ESNew/api/EsferiatServGetQuestChoices"
            }
        }
        
        var queryParamters: String? {
            switch self {
            case .getUserInfo, .getMonthQuestion, .submitMonthQuestionAnswer, .updateEmail, .updatePassword, .getConfirmationCode, .deleteAccount, .fetchCards, .showSupport, .downloadPDF, .getPartners, .getMonthQuestionOptions:
                return nil
            }
        }
        
        var baseUrl: URL {
            return AppConfigurations.BaseUrl
        }
        
        var method: HTTPMethod {
            switch self {
            //                   case .forgetPassword, .createAccount:
            //                       return .post
            case .getUserInfo, .getMonthQuestion , .submitMonthQuestionAnswer, .updateEmail, .updatePassword, .getConfirmationCode, .deleteAccount, .fetchCards, .showSupport, .downloadPDF, .getPartners, .getMonthQuestionOptions:
                return .get
            }
        }
        
        var parameters: Parameters? {
            switch self {
            case let .getUserInfo(api):
                return ["Sub_ID": api.Sub_ID]//api.params?.getParamsAsJson()
            case let .getMonthQuestion(api):
                return ["Sub_ID": api.Sub_ID]
            case let .submitMonthQuestionAnswer(api):
                return ["Sub_ID": api.Sub_ID, "Q_ID": "", "SubQ_Answer": api.answer]
            case let .updateEmail(api):
                return ["NewEmail": api.email, "Sub_ID": api.Sub_ID]
            case let .updatePassword(api):
                return ["NewPassword": api.email, "Sub_ID": api.Sub_ID]
            case let .getConfirmationCode(api):
                return ["UMail": api.email]
            case let .deleteAccount(api):
                return ["id":api.Sub_ID]
            case .fetchCards:
                return ["type":"json"]
            case let .showSupport (api):
                return ["SupportType": api.supportType, "SupportNote": api.noteType, "SupportEmail":api.email, "SupportPhone":api.phone]
            case .downloadPDF:
                return ["type":"json"]
            case .getPartners:
                return ["type":"json"]
            case let .getMonthQuestionOptions(api):
                return ["Q_ID": api.qID, "type":"json"]
            }
        }
    }
}


extension QuestionsApi {
    
    func getUserInfo(id: String) -> Observable<UserDetailsHome> {
        
        Sub_ID = id
        
        return Observable.create { observable in
            
            self.fireRequestWithCustomResponse(requestable: APIRouter.getUserInfo(self)) { (result, error) in
                
                if error != nil {
                    observable.onError(error!)
                } else if let jsonData = try? JSONSerialization.data(withJSONObject: result ?? [:] , options: .prettyPrinted) {
                    if let response = try? JSONDecoder().decode(UserDetailsHome.self, from: jsonData) {
                        observable.onNext(response)
                        observable.onCompleted()
                    } else {
                        observable.onError(NSError(domain: "حدث خطاء ما", code: 3841, userInfo: nil))
                    }
                }
                
            }
            return Disposables.create()
        }
    }
    
    
    
    func getMonthQuestion(id: String) -> Observable<Question> {
        
        Sub_ID = id
        
        return Observable.create { observable in
            
            self.fireRequestWithCustomResponse(requestable: APIRouter.getMonthQuestion(self)) { (result, error) in
                
                if error != nil {
                    observable.onError(error!)
                } else if let jsonData = try? JSONSerialization.data(withJSONObject: result ?? [:] , options: .prettyPrinted) {
                    if let response = try? JSONDecoder().decode(Question.self, from: jsonData) {
                        observable.onNext(response)
                        observable.onCompleted()
                    } else {
                        observable.onError(NSError(domain: "حدث خطاء ما", code: 3841, userInfo: nil))
                    }
                }
                
            }
            return Disposables.create()
        }
    }
    
    func submitMonthQuestionAnswer(id: String, answer: String, qID: String) -> Observable<Bool> {
        
        Sub_ID = id
        self.answer = answer
        self.qID = qID
        return Observable.create { observable in
            
            self.fireRequestWithCustomResponse(requestable: APIRouter.getMonthQuestion(self)) { (result, error) in
                
                if error != nil {
                    observable.onError(error!)
                } else if let jsonData = try? JSONSerialization.data(withJSONObject: result ?? [:] , options: .prettyPrinted) {
                    if let response = result {
                        observable.onNext(true)
                        observable.onCompleted()
                    } else {
                        observable.onError(NSError(domain: "حدث خطاء ما", code: 3841, userInfo: nil))
                    }
                }
                
            }
            return Disposables.create()
        }
    }
    
    func updateEmail(id: String, email: String) -> Observable<Bool> {
        
        Sub_ID = id
        self.email = email
        return Observable.create { observable in
            
            self.fireRequestWithCustomResponse(requestable: APIRouter.updateEmail(self)) { (result, error) in
                
                if error != nil {
                    observable.onError(error!)
                } else if let jsonData = try? JSONSerialization.data(withJSONObject: result ?? [:] , options: .prettyPrinted) {
                    if let response = result {
                        observable.onNext(true)
                        observable.onCompleted()
                    } else {
                        observable.onError(NSError(domain: "حدث خطاء ما", code: 3841, userInfo: nil))
                    }
                }
                
            }
            return Disposables.create()
        }
    }
    
    func updatePassword(id: String, password: String) -> Observable<Bool> {
        
        Sub_ID = id
        self.password = password
        return Observable.create { observable in
            
            self.fireRequestWithCustomResponse(requestable: APIRouter.updatePassword(self)) { (result, error) in
                
                if error != nil {
                    observable.onError(error!)
                } else if let jsonData = try? JSONSerialization.data(withJSONObject: result ?? [:] , options: .prettyPrinted) {
                    if let response = result {
                        observable.onNext(true)
                        observable.onCompleted()
                    } else {
                        observable.onError(NSError(domain: "حدث خطاء ما", code: 3841, userInfo: nil))
                    }
                }
                
            }
            return Disposables.create()
        }
    }
    
    func getConfirmationCode(email: String) -> Observable<String> {
        
        self.email = email
        return Observable.create { observable in
            
            self.fireRequestWithCustomResponse(requestable: APIRouter.getConfirmationCode(self)) { (result, error) in
                
                if error != nil {
                    observable.onError(error!)
                } else if let jsonData = try? JSONSerialization.data(withJSONObject: result ?? [:] , options: .prettyPrinted) {
                    if let code = result?["AuthCode"] {
                        observable.onNext(code as! String)
                        observable.onCompleted()
                    } else {
                        observable.onError(NSError(domain: "حدث خطاء ما", code: 3841, userInfo: nil))
                    }
                }
                
            }
            return Disposables.create()
        }
    }
    
    func deleteAccount(email: String) -> Observable<Bool> {
        
        self.email = email
        return Observable.create { observable in
            
            self.fireRequestWithCustomResponse(requestable: APIRouter.deleteAccount(self)) { (result, error) in
                
                if error != nil {
                    observable.onError(error!)
                } else if let jsonData = try? JSONSerialization.data(withJSONObject: result ?? [:] , options: .prettyPrinted) {
                    if let response = result {
                        observable.onNext(true)
                        observable.onCompleted()
                    } else {
                        observable.onError(NSError(domain: "حدث خطاء ما", code: 3841, userInfo: nil))
                    }
                }
                
            }
            return Disposables.create()
        }
    }
    
    func fetchCards()  -> Observable<[Card]> {
        
        return Observable.create { observable in
            
            self.fireRequestWithCustomResponse(requestable: APIRouter.fetchCards, IncludingDate: false) { (result, error) in
                let data = (result!["data"] as? Array<Any>) as? [[String:Any]]
                if error != nil {
                    observable.onError(error!)
                } else if let jsonData = try? JSONSerialization.data(withJSONObject: data ?? [:] , options: .prettyPrinted) {
                    if let response = try? JSONDecoder().decode([Card].self, from: jsonData) {
                        observable.onNext(response)
                        observable.onCompleted()
                    } else {
                        observable.onError(NSError(domain: "حدث خطاء ما", code: 3841, userInfo: nil))
                    }
                }
                
            }
            return Disposables.create()
        }
    }
    
    func showSupport()  -> Observable<Bool> {
        
        return Observable.create { observable in
            
            self.fireRequestWithCustomResponse(requestable: APIRouter.showSupport(self)) { (result, error) in
                
                if error != nil {
                    observable.onError(error!)
                } else if let jsonData = try? JSONSerialization.data(withJSONObject: result ?? [:] , options: .prettyPrinted) {
                    if let response = try? JSONDecoder().decode(SignUpResponse.self, from: jsonData) {
                        if response.Code.lowercased().contains("done") {
                            observable.onNext(true)
                            observable.onCompleted()
                        } else {
                            observable.onError(NSError(domain: "حدث خطاء ما", code: 3841, userInfo: nil))
                        }
                    } else {
                        observable.onError(NSError(domain: "حدث خطاء ما", code: 3841, userInfo: nil))
                    }
                }
                
            }
            return Disposables.create()
        }
    }
    
    func downloadPDF() ->Observable<String> {
        return Observable.create { observable in
            
            self.fireRequestWithCustomResponse(requestable: APIRouter.downloadPDF) { (result, error) in
                
                if error != nil {
                    observable.onError(error!)
                } else if let url = result?["Book_PDFPath"] {
                    observable.onNext((url as? String) ?? "")
                    observable.onCompleted()
                } else {
                    observable.onError(NSError(domain: "حدث خطاء ما", code: 3841, userInfo: nil))
                }
                
            }
            return Disposables.create()
        }
        
    }
    
    
    func getPartners() -> Observable<[Partner]> {
        
        return Observable.create { observable in
            
            self.fireRequestWithCustomResponse(requestable: APIRouter.getPartners, IncludingDate: false) { (result, error) in
                let data = (result!["data"] as? Array<Any>) as? [[String:Any]]
                if error != nil {
                    observable.onError(error!)
                } else if let jsonData = try? JSONSerialization.data(withJSONObject: data ?? [:] , options: .prettyPrinted) {
                    if let response = try? JSONDecoder().decode([Partner].self, from: jsonData) {
                        observable.onNext(response)
                        observable.onCompleted()
                    } else {
                        observable.onError(NSError(domain: "حدث خطاء ما", code: 3841, userInfo: nil))
                    }
                }
                
            }
            return Disposables.create()
        }
    }
    
    func getMonthQuestionOptions() -> Observable<[MCQuestion]> {
        
        return Observable.create { observable in
            
            self.fireRequestWithCustomResponse(requestable: APIRouter.getMonthQuestionOptions(self), IncludingDate: false) { (result, error) in
                let data = (result!["data"] as? Array<Any>) as? [[String:Any]]
                if error != nil {
                    observable.onError(error!)
                } else if let jsonData = try? JSONSerialization.data(withJSONObject: data ?? [:] , options: .prettyPrinted) {
                    if let response = try? JSONDecoder().decode([MCQuestion].self, from: jsonData) {
                        observable.onNext(response)
                        observable.onCompleted()
                    } else {
                        observable.onError(NSError(domain: "حدث خطاء ما", code: 3841, userInfo: nil))
                    }
                }
                
            }
            return Disposables.create()
        }
    }

}

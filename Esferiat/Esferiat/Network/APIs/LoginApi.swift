//
//  LoginApi.swift
//  Coach You
//
//  Created by Mohammed Salah on 11/05/2020.
//  Copyright © 2020 msalah. All rights reserved.
//

import Alamofire
import PromiseKit
import RxSwift
import UIKit

class LoginApi: Api {
    var email = ""

    init(params: RequestParamters?) {
        super.init()
        self.params = params
    }
    
    enum APIRouter: Requestable {
           case login (LoginApi)
            case forgetPassword(LoginApi)
            case createAccount(LoginApi)

           var path: String {
               switch self {
               case .login:
                   return "ESNew/api/EsferiatServLogin"
                case .forgetPassword:
                    return "public/accounts/forget_password"
               case .createAccount:
                    return "ESNew/api/EsferiatServ"
               }
           }

           var queryParamters: String? {
               switch self {
               case .login, .forgetPassword, .createAccount:
                   return nil
               }
           }

           var baseUrl: URL {
               return AppConfigurations.BaseUrl
           }

           var method: HTTPMethod {
               switch self {
               case .forgetPassword, .createAccount:
                   return .post
               case .login :
                return .get
               }
           }

           var parameters: Parameters? {
               switch self {
               case let .login(api):
                   return api.params?.getParamsAsJson()
               case let .forgetPassword(api):
                return ["email":api.email.lowercased()]
               case let .createAccount(api) :
                return api.params?.getParamsAsJson()
               }
           }
       }
}

extension LoginApi {
    
    func login() -> Observable<UserInfo> {
        
        return Observable.create { observable in
            
            self.fireRequestWithCustomResponse(requestable: APIRouter.login(self)) { (result, error) in
                
                if error != nil {
                    observable.onError(error!)
                }else if result == nil {
                    observable.onError(NSError(domain: "تاكد من بياناتك", code: 3841, userInfo: nil))
                } else if let jsonData = try? JSONSerialization.data(withJSONObject: result ?? [:] , options: .prettyPrinted) {
                    if let response = try? JSONDecoder().decode(UserInfo.self, from: jsonData) {
                        observable.onNext(response)
                        observable.onCompleted()
                    } else {
                        observable.onError(NSError(domain: "حدث خطاء ما", code: 3841, userInfo: nil))
                    }
                }
                
            }
            return Disposables.create()
        }
    }
    
func forgetPass(_ email:String) {// -> Observable<NSObject> {
        self.email = email
        
//        return Observable.create { observable in
//            let callBack: Promise<NSObject> = self.fireRequestWithSingleResponse(requestable: APIRouter.forgetPassword(self))
//
//            callBack.get { respose in
//                    observable.onNext(respose)
//                    observable.onCompleted()
//            }.catch { error in
//                observable.onError(error)
//            }
//            return Disposables.create()
//        }

    }
    
    func createAccount() -> Observable<SignUpResponse> {
        let signUpParams = params as! SignupRequesrParams
            let parameters: [String: Any] = [
                "Sub_Name" : signUpParams.Sub_Name,
                "Sub_2Name":signUpParams.Sub_2Name,
                "Sub_3Name":signUpParams.Sub_3Name,
                "Sub_Email":signUpParams.Sub_Email,
                "Phone":signUpParams.Phone,
                "Nationality":signUpParams.Nationality,
                "MaleFemmele":signUpParams.MaleFemmele,
            ]
        
        
        
        return Observable.create { observable in
            
            Alamofire.request("http://mutaweronteam.com/ESNew/api/EsferiatServ", method: .post, parameters: self.params?.getParamsAsJson()).responseJSON { (result) in
                let value = result.result.value//.//replacingOccurrences(of: "\\", with: "")
                let json = self.convertToDictionary(text: value as! String ) ?? [:]
                let data = (json["data"] as? Array<Any>)?.first as? [String: Any]
                print(result)
                if let jsonData = try? JSONSerialization.data(withJSONObject: data ?? [:] , options: .prettyPrinted) {
                    if let response = try? JSONDecoder().decode(SignUpResponse.self, from: jsonData) {
                        observable.onNext(response)
                        observable.onCompleted()
                    } else {
                        observable.onError(NSError(domain: "حدث خطاء ما", code: 3841, userInfo: nil))
                    }
                }
            }
            return Disposables.create()
        }

    }
}

//
//
//  Api.swift
//  Near Me
//
//  Created by Mohammed Salah on 12/04/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//
import Alamofire
import PromiseKit
import RxSwift
import UIKit

class Api: NSObject {
    var params: RequestParamters?
    var requestType: Requestable?
    var result = ""
    var requestsId = ""
    
    func getApiName() -> String {
        return ""
    }
    
    func beginBackgroundUpdateTask() -> UIBackgroundTaskIdentifier {
        return UIApplication.shared.beginBackgroundTask(expirationHandler: ({}))
    }
    
    func endBackgroundUpdateTask(taskID: UIBackgroundTaskIdentifier) {
        UIApplication.shared.endBackgroundTask(taskID)
    }
    
    func cancel() {
        ServiceManager.shared.cancelRequestWithID(requestID: requestsId)
    }
}

extension Api {
    func fireRequestWithSingleResponse<T: Codable>(requestable: Requestable) -> Promise<T> {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        //        let taskId = beginBackgroundUpdateTask()
        
        return Promise<T> { seal in
            let completionHandler: (DataResponse<T>) -> Void = { response in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                //                self.endBackgroundUpdateTask(taskID: taskId)
                guard response.error == nil else {
                    if (response.error as? URLError) != nil {
                        // no internet connection
                        let errorN = CustomError(code: "00-000", message: "Sorry, Please check your internet connection")
                        seal.reject(errorN)
                        return
                    }
                    seal.reject(CustomError.getError(error: response.error!))
                    return
                }
                guard response.value != nil else {
                    _ = NSError(domain: "JSONResponseError", code: 3841, userInfo: nil)
                    seal.reject(CustomError.getError(error: response.error!))
                    return
                }
                seal.fulfill(response.result.value!)
            }
            
            requestable.request(requestID: requestsId, with: completionHandler)
        }
    }
    
    func fireRequestWithSingleResponseRX<T: Codable>(requestable: Requestable) -> Observable<T> {
        return Observable.create { observable in
            let callBack: Promise<T> = self.fireRequestWithSingleResponse(requestable: requestable)
            
            callBack.get { respose in
                observable.onNext(respose)
                observable.onCompleted()
            }.catch { error in
                observable.onError(error)
            }
            return Disposables.create()
        }
    }
    
    func fireRequestWithRXResponse<T: Codable>(requestable: Requestable) -> Observable<T> {
        return Observable.create { observable in
            
            self.fireRequestWithCustomResponse(requestable: requestable) { (result, error) in
                
                if error != nil {
                    observable.onError(error!)
                } else if let jsonData = try? JSONSerialization.data(withJSONObject: result ?? "", options: .prettyPrinted) {
                    if let response = try? JSONDecoder().decode(T.self, from: jsonData) {
                        observable.onNext(response)
                        observable.onCompleted()
                    } else {
                        observable.onError(NSError(domain: "Somthing went wrong", code: 3841, userInfo: nil))
                    }
                }
                
            }
            return Disposables.create()
        }
    }
    
    func fireRequestWithCustomResponse(requestable: Requestable, IncludingDate: Bool = true, complition: @escaping (([String: Any]?, Error?) -> Void))  {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        //        let taskId = beginBackgroundUpdateTask()
        
        let completionHandler: (DataResponse<String>) -> Void = { response in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            //            self.endBackgroundUpdateTask(taskID: taskId)
            guard response.error == nil else {
                if let err = response.error as? URLError {
                    // no internet connection
                    complition(nil, err)
                    return
                }
                
                let errorN = CustomError(code: "0X0", message: "Somwthing went wrong")
                complition(nil, errorN)
                return
            }
            guard response.value != nil else {
                _ = NSError(domain: "JSONResponseError", code: 3841, userInfo: nil)
                complition(nil, response.error! )
                return
            }
            let resultAsStr = response.result.value
            let result: [String: Any] = self.convertToDictionary(text: resultAsStr ?? "") ?? [:]//resultOK as? [String: Any] ?? [:]
            
            if IncludingDate {
                let data = (result["data"] as? Array<Any>)?.first as? [String: Any]
                complition(data, nil)
                return
            }
            
            complition(result, nil)
        }
        requestable.request(requestID: requestsId, with: completionHandler)
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    //    func uploadImage(image: UIImage,compilation: @escaping ((UploadImageResponse?,Error?)->Void)) {
    //
    //        var url = AppConfigurations.BaseUrl
    //        url = url.appendingPathComponent("private/images/upload_new")
    ////        url.head
    //
    //        if let data = image.jpegData(compressionQuality: 0.5) {
    //            Alamofire.upload(multipartFormData: { multiPartData in
    ////                if let param = self.params?.getParamsAsJson() {
    ////                    for (key, value) in param {
    ////                        multiPartData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
    ////                    }
    ////                }
    //
    //                multiPartData.append(data, withName: "image" , fileName: String.randomString(length: 16), mimeType: "image/jpg")
    //            }, to: url, method: .post, headers: [ServerKeys.Auth: CUserDefaults.userAuth, "Content-type":"multipart/form-data"]) { (result) in
    //                switch result {
    //                case .success(let upload, _, _):
    //
    //                    upload.uploadProgress(closure: { (progress) in
    //                        //Print progress
    //                        print("uploading \(progress)")
    //
    //                    })
    //
    //                    upload.responseJSON { (response) in
    //                        print(response)
    //                        if let ero = response.error {
    //                            let error = CustomError(code: "0X0000", message: ero.message)
    //                            compilation(nil, error)
    //                            return;
    //                        }
    //
    //                        let error = CustomError(code: "0X0000", message: "Something went wrong")
    //
    //                        if let jsonData = try? JSONSerialization.data(withJSONObject: response.result.value ?? "", options: .prettyPrinted) {
    //                            if let uploadResponse = try? JSONDecoder().decode(UploadImageResponse.self, from: jsonData) {
    //                                compilation(uploadResponse, nil)
    //                                return;
    //                            }
    //                            compilation(nil, error)
    //                            return;
    //                        }
    //                        compilation(nil, error)
    //                        return;
    //                    }
    //
    //                case .failure(let error):
    //                    compilation(nil, error)
    //                }
    //            }
    //        } else {
    //            compilation(nil, CustomError(code: "0X00", message: "Something went wrong"))
    //        }
    //    }
}

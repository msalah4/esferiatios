//
//  UpdateInfoConfirmationCodeViewController.swift
//  Esferiat
//
//  Created by Mohammed Salah on 22/08/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//

import UIKit
import RxSwift

enum UpdateInfoType {
    case Email
    case Password
}

class UpdateInfoConfirmationCodeViewController: BaseViewController {

    @IBOutlet weak var codeTF: UITextField!
    var infoType = UpdateInfoType.Email
    var confirmationCode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let api = QuestionsApi(params: nil)
        
        api.getConfirmationCode(email: ASUserDefaults.currentUser?.Sub_Code ?? "").subscribe(onNext: { [weak self] (value) in
            self?.confirmationCode = value
        }).disposed(by: bag)
    }
    
    @IBAction func didPressSend(_ sender: Any) {
        if confirmationCode == codeTF.text {
            let updateVC = storyboard?.instantiateViewController(withIdentifier: "UpdateInfoViewController") as! UpdateInfoViewController
            updateVC.infoType = infoType
            self.show(updateVC, sender: nil)
        } else {
            showErrorWith(message: "")
        }
    }
}

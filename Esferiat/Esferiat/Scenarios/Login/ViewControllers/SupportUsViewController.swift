//
//  SupportUsViewController.swift
//  Esferiat
//
//  Created by Mohammed Salah on 30/10/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//

import UIKit

class SupportUsViewController: BaseViewController {
    
    @IBOutlet weak var phoneInputView: InputFieldView!
    @IBOutlet weak var emailInputView: InputFieldView!
    @IBOutlet weak var supportTextView: UITextView!
    @IBOutlet weak var typeOfSupport: UISegmentedControl!
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func didPressSubmit(_ sender: Any) {
        
        if phoneInputView.inputedValue().isEmpty || emailInputView.inputedValue().isEmpty || supportTextView.text.isEmpty {
            AlertView(title: "خطاء", message: "كل الحقول مطلوبة").withOneOption(title: "حسنا").show(view: self)
            return
        }
        
        let api = QuestionsApi(params: nil)
        api.phone = phoneInputView.inputedValue()
        api.email = emailInputView.inputedValue()
        api.supportType = typeOfSupport.titleForSegment(at: typeOfSupport.selectedSegmentIndex) ?? ""
        api.noteType = supportTextView.text ?? ""
        
        showDefaultLoader()
        api.showSupport().subscribe(onNext: { [weak self](response) in
            self?.hideDefaultLoader()
            if let strongSelf = self{
                AlertView(title: "", message: "تم ارسال مقترحكم بنجاح").withOneOption(title: "حسنا").show(view: strongSelf)
            }
        }, onError: { [weak self] (error) in
            self?.hideDefaultLoader()
            self?.showErrorWith(message: error.localizedDescription)
        }).disposed(by: bag)
        
    }
    

}

//
//  PartenersViewController.swift
//  Esferiat
//
//  Created by Mohammed Salah on 31/10/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//

import UIKit

class PartenersViewController: BaseViewController {
    
    @IBOutlet weak var partnersTableView: UITableView!
    var listOfPartners: [Partner] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        partnersTableView.register(UINib(nibName: "PartnerCell", bundle: nil), forCellReuseIdentifier: "Cell")//register(PartnerCell.self, forCellReuseIdentifier: "Cell")
        partnersTableView.tableFooterView = UIView(frame: CGRect.zero)
        partnersTableView.separatorStyle = .none
        
        showDefaultLoader()
        let api = QuestionsApi(params: nil)
        api.getPartners().subscribe(onNext: { [weak self](response) in
            self?.hideDefaultLoader()
            self?.listOfPartners = response
            self?.partnersTableView.reloadData()
        }, onError: { [weak self] (error) in
            self?.hideDefaultLoader()
            self?.showErrorWith(message: error.localizedDescription)
        }).disposed(by: bag)
        
    }

}

extension PartenersViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfPartners.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PartnerCell
        let partner = listOfPartners[indexPath.row]

        if let url = URL(string: partner.P_Path ?? "") {
            cell.partnerImage.af_setImage(withURL: url)
        }
        cell.partnerName.text = partner.P_Name
        cell.partnerDesc.text = partner.P_Note
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
}

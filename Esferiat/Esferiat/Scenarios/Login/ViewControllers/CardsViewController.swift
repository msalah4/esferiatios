//
//  CardsViewController.swift
//  Esferiat
//
//  Created by Mohammed Salah on 26/10/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//

import UIKit

class CardsCell: UITableViewCell {
    var imageV: UIImageView! = UIImageView()
    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        imageV.frame = self.bounds
//        self.addSubview(imageV)
//        imageV.contentMode = .scaleAspectFit
//    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        imageV.frame = self.bounds
        self.addSubview(imageV)
        imageV.contentMode = .scaleAspectFit
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class CardsViewController: BaseViewController {
    
    @IBOutlet weak var cardsTableView: UITableView!
    var listOfCards: [Card] = []
    var downloadUrl = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        cardsTableView.register(CardsCell.self, forCellReuseIdentifier: "Cell")
        cardsTableView.tableFooterView = UIView(frame: CGRect.zero)
        cardsTableView.separatorStyle = .none
        
        showDefaultLoader()
        let api = QuestionsApi(params: nil)
        api.fetchCards().subscribe(onNext: { [weak self](response) in
            self?.hideDefaultLoader()
            self?.listOfCards = response
            self?.cardsTableView.reloadData()
        }, onError: { [weak self] (error) in
            self?.hideDefaultLoader()
            self?.showErrorWith(message: error.localizedDescription)
        }).disposed(by: bag)
        
        api.downloadPDF().subscribe(onNext: { [weak self](response) in
            self?.downloadUrl = response
        }, onError: { [weak self] (error) in
            self?.showErrorWith(message: error.localizedDescription)
        }).disposed(by: bag)
        
    }
    
    @IBAction func didPressDownload(_ sender: Any) {
        if let url = URL(string: downloadUrl) {
            UIApplication.shared.open(url)
        }
    }
    

}

extension CardsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfCards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CardsCell
        let card = listOfCards[indexPath.row]

        if let url = URL(string: card.C_Path ?? "") {
            cell.imageV.af_setImage(withURL: url)
            cell.imageV.frame = cell.bounds
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cardsTableView.frame.size.height - 25
    }
    
}

//
//  UpdateInfoViewController.swift
//  Esferiat
//
//  Created by Mohammed Salah on 22/08/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//

import UIKit

class UpdateInfoViewController: BaseViewController {
    
    @IBOutlet weak var emailTF: UITextField!
    var infoType = UpdateInfoType.Email

    override func viewDidLoad() {
        super.viewDidLoad()

        if infoType == .Password {
            emailTF.placeholder = "الباسورد"
        }
        
    }
    
    @IBAction func didPressSend(_ sender: Any) {
        
        let api = QuestionsApi(params: nil)
        showDefaultLoader()
        
        if infoType == .Password {
            api.updatePassword(id: ASUserDefaults.currentUser?.Sub_ID ?? "", password: emailTF.text ?? "").subscribe(onNext: { [weak self] (value) in
                self?.hideDefaultLoader()
                self?.navigationController?.popToRootViewController(animated: true)
            }).disposed(by: bag)
        } else {
            api.updateEmail(id: ASUserDefaults.currentUser?.Sub_ID ?? "", email: emailTF.text ?? "").subscribe(onNext: { [weak self] (value) in
                self?.hideDefaultLoader()
                self?.navigationController?.popToRootViewController(animated: true)
            }).disposed(by: bag)
        }
    }
    
}

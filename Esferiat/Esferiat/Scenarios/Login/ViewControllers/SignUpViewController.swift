//
//  SignUpViewController.swift
//  Esferiat
//
//  Created by Mohammed Salah on 25/07/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//

import UIKit

class SignUpViewController: BaseViewController {

    var validator:Validator = Validator()
    var viewModel = SignUpViewModel()
    
    @IBOutlet weak var firstNameInputView: InputFieldView!
    @IBOutlet weak var lastNameInputView: InputFieldView!
    @IBOutlet weak var surNameInputView: InputFieldView!
    @IBOutlet weak var phoneInputView: InputFieldView!
    @IBOutlet weak var passwordInputView: InputFieldView!
    @IBOutlet weak var passwordConfInputView: InputFieldView!
    @IBOutlet weak var genderInputView: InputFieldView!
    @IBOutlet weak var birthdateInputView: InputFieldView!
    @IBOutlet weak var emailInputView: InputFieldView!
    @IBOutlet weak var emailConfInputView: InputFieldView!
    @IBOutlet weak var nationalityInputView: InputFieldView!
    @IBOutlet weak var howInputView: InputFieldView!
    @IBOutlet weak var genderSegment: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.onErrorSubject.subscribe(onNext: { (error) in
            self.hideDefaultLoader()
            self.showErrorWith(message: error)
        }).disposed(by: bag)
        
        viewModel.onSignedUpSubject.subscribe(onNext: { (result) in
            self.hideDefaultLoader()
            self.showErrorWith(message: "Code : \(result.Code)", title: "تم التسجيل")
            print("Code : \(result.Code)")
        }).disposed(by: bag)
        
        validator.registerInputView(firstNameInputView, rules: [RequiredRule(message: "الاسم الاول اجباري")])
        validator.registerInputView(lastNameInputView, rules: [RequiredRule(message: "الاسم الاخير اجباري")])
        validator.registerInputView(surNameInputView, rules: [RequiredRule(message: "اجباري")])
//        validator.registerInputView(phoneInputView, rules: [RequiredRule(message: "")])
        validator.registerInputView(passwordInputView, rules: [RequiredRule(message: "الباسورد اجباري")])
        validator.registerInputView(passwordConfInputView, rules: [RequiredRule(message: "تاكيد الباسورد اجباري")])
//        validator.registerInputView(genderInputView, rules: [RequiredRule(message: "")])
//        validator.registerInputView(birthdateInputView, rules: [RequiredRule(message: "")])
        validator.registerInputView(emailInputView, rules: [RequiredRule(message: "الايميل اجباري")])
        validator.registerInputView(emailConfInputView, rules: [RequiredRule(message: "تاكيد الايميل اجباري")])
//        validator.registerInputView(nationalityInputView, rules: [RequiredRule(message: "")])
//        validator.registerInputView(howInputView, rules: [RequiredRule(message: "")])

        
        // For Test
//        firstNameInputView.setInputValue("محمد")
//        lastNameInputView.setInputValue("صلاح")
//        surNameInputView.setInputValue("صلاج")
//        phoneInputView.setInputValue("٠١٢٣٤٥٦٧٨٩٠١١")
//        passwordInputView.setInputValue("12345678")
//        passwordConfInputView.setInputValue("12345678")
////        genderInputView.setInputValue("ذكر")
//        birthdateInputView.setInputValue("10/04/1992")
//        emailInputView.setInputValue("mohamed.salah.2011+50@gmail.com")
//        emailConfInputView.setInputValue("mohamed.salah.2011+50@gmail.com")
//        nationalityInputView.setInputValue("مصر")
//        howInputView.setInputValue("App")
    }
    
    @IBAction func didPressContinue(_ sender: Any) {
        validator.validate(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
}

extension SignUpViewController : ValidationDelegate {
    func validationSuccessful() {
        
        viewModel.params.Sub_Name = firstNameInputView.inputedValue()
        viewModel.params.Sub_2Name = lastNameInputView.inputedValue()
        viewModel.params.Sub_3Name = surNameInputView.inputedValue()
        viewModel.params.Sub_Email = emailInputView.inputedValue()
        viewModel.params.Sub_Password = passwordInputView.inputedValue()
//        viewModel.params.BirthDate = birthdateInputView.inputedValue()
        viewModel.params.MaleFemmele = genderSegment.titleForSegment(at: genderSegment.selectedSegmentIndex)//genderInputView.inputedValue()
        viewModel.params.Nationality = ""//howInputView.inputedValue()
        viewModel.params.Phone = phoneInputView.inputedValue()
        viewModel.params.Certifiate = howInputView.inputedValue()
        
        self.showDefaultLoader()
        viewModel.signupNewUser()
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        
    }
    
}

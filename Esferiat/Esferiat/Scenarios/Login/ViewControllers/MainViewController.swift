//
//  MainViewController.swift
//  Esferiat
//
//  Created by Mohammed Salah on 17/08/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//

import UIKit

class MainViewController: BaseViewController {
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var numberOfSubscriptionsLabel: UILabel!
    @IBOutlet weak var rightAnswersLabel: UILabel!
    @IBOutlet weak var wrongAnswersLable: UILabel!
    @IBOutlet weak var langButton: UIButton!
    
    lazy var loggedInUser = ASUserDefaults.currentUser
    let viewModel = HomeViewModel()

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let uName = loggedInUser?.SubName {
            userNameLabel.text = uName
        }
        
        viewModel.onErrorSubject.subscribe(onNext: { [weak self] (error) in
            self?.hideDefaultLoader()
            self?.showErrorWith(message: error)
        }).disposed(by: bag)
        
        viewModel.onFetchUserInfoSubject.subscribe(onNext: { [weak self] (result) in
            self?.numberOfSubscriptionsLabel.text = result.Subscriptions
            self?.rightAnswersLabel.text = result.TrueAnswers
            self?.wrongAnswersLable.text = result.FalseAnswers
        }).disposed(by: bag)
        
        viewModel.onDeleteAccountSubject.subscribe(onNext: { [weak self] (error) in
            self?.hideDefaultLoader()
            self?.navigationController?.popToRootViewController(animated: true)
        }).disposed(by: bag)
        
        if LocalizeService.shared.selectedLanguage == .arabic {
            langButton.setTitle("EN", for: .normal)
        } else {
            langButton.setTitle("AR", for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if loggedInUser?.Sub_Code == "-10", loggedInUser?.Sub_ID == "-10" {
            numberOfSubscriptionsLabel.text = "10"
            rightAnswersLabel.text = "7"
            wrongAnswersLable.text = "3"
            ASUserDefaults.currentUser = nil
        } else {
            viewModel.fetchHomeUserInfo()
        }
        
    }
    

    @IBAction func didPressShowQuestion(_ sender: Any) {
        let qVC = storyboard?.instantiateViewController(withIdentifier: "PreviewQuestionViewController") as! PreviewQuestionViewController
        qVC.viewModel = viewModel
        self.show(qVC, sender: nil)
    }
    
    @IBAction func didPressChangeEmail(_ sender: Any) {
        let updateVC = storyboard?.instantiateViewController(withIdentifier: "UpdateInfoConfirmationCodeViewController") as! UpdateInfoConfirmationCodeViewController
        updateVC.infoType = .Email
        self.show(updateVC, sender: nil)
    }
    
    @IBAction func didPressEditPassword(_ sender: Any) {
        let updateVC = storyboard?.instantiateViewController(withIdentifier: "UpdateInfoConfirmationCodeViewController") as! UpdateInfoConfirmationCodeViewController
        updateVC.infoType = .Password
        self.show(updateVC, sender: nil)
    }
    
    @IBAction func didPressLogout(_ sender: Any) {
        ASUserDefaults.currentUser = nil
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didPressDelete(_ sender: Any) {
        ASUserDefaults.currentUser = nil
        showDefaultLoader()
        viewModel.deleteAccount()
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didPressChangeLange(_ sender: Any) {
        if LocalizeService.shared.selectedLanguage == .arabic {
            LocalizeService.shared.setLanguage(.english)
        } else {
            LocalizeService.shared.setLanguage(.arabic)
        }
        UIApplication.shared.didChangeLang()
        
    }
    
}

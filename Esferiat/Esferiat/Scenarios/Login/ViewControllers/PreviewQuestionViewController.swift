//
//  PreviewQuestionViewController.swift
//  Esferiat
//
//  Created by Mohammed Salah on 19/08/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//

import UIKit

class PreviewQuestionViewController: BaseViewController {
    
//    @IBOutlet weak var qImageView: UIImageView!
//    @IBOutlet weak var answerTF: CUTextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var questionLabel: UILabel!
    var viewModel:HomeViewModel!
    var selectedIndex = -1
    
    var monthlyQuestion: Question?
    var MCQs: [MCQuestion] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        viewModel.onErrorSubject.subscribe(onNext: { [weak self] (error) in
            self?.hideDefaultLoader()
            self?.showErrorWith(message: error)
        }).disposed(by: bag)
        
        viewModel.onSubmitMonthlyQSubject.subscribe(onNext: { [weak self] (value) in
            self?.hideDefaultLoader()
            self?.showErrorWith(message: "تم ارسال اجابتك بنجاح", title: "")
        }).disposed(by: bag)
        
        showDefaultLoader()
        viewModel.onFetchMonthlyQSubject.subscribe(onNext: { [weak self] (question) in
            //            if question.Q_Answer == "-1" {
            //                self?.answerTF.isEnabled = false
            //                self?.showErrorWith(message: "لا يمكن تعديل اجابتك")
            //                self?.navigationController?.popViewController(animated: true)
            //            }
            self?.monthlyQuestion = question
            if LocalizeService.shared.selectedLanguage == .arabic {
                self?.questionLabel.text = question.Q_Description
            } else {
                self?.questionLabel.text = question.Q_DescriptionEn
            }
            
            self?.viewModel.fetchMonthlyQuestionOptions(id: question.Q_ID)
        }).disposed(by: bag)
        
        viewModel.onFetchMonthlyQOptionsSubject.subscribe(onNext: { [weak self] (mcqs) in
            self?.hideDefaultLoader()
            self?.MCQs = mcqs
            self?.tableView.reloadData()
        }).disposed(by: bag)
        
        viewModel.fetchMonthlyQuestion()
    }
    
    
    @IBAction func didPressASubmitAnswer(_ sender: Any) {
        
        //        if monthlyQuestion?.Q_Answer == "-1" {
        showDefaultLoader()
        viewModel.submitMonthlyQuestion(answer: MCQs[selectedIndex].QAnswerID ?? "", qID: monthlyQuestion?.Q_ID ?? "")
        //        }
    }
    
    
}


extension PreviewQuestionViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MCQs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        if let deuqCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? UITableViewCell {
            cell = deuqCell
        } else {
            cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        }
        
        let mcq = MCQs[indexPath.row]
        let urlString = mcq.Q_Choice?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""

        if let url = URL(string: urlString) {
            cell.imageView?.af_setImage(withURL: url)
        }
        
//        cell.isSelected = indexPath.row == selectedIndex
        cell.setSelected(indexPath.row == selectedIndex, animated: true)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        print(selectedIndex)
//        self.tableView.reloadData()
    }
}

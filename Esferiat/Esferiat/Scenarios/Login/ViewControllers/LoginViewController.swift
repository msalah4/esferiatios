//
//  LoginViewController.swift
//  Esferiat
//
//  Created by Mohammed Salah on 25/07/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {
    
    var validator:Validator = Validator()
    var viewModel = SignUpViewModel()
    
    @IBOutlet weak var userNameInputView: InputFieldView!
    @IBOutlet weak var passwordInputView: InputFieldView!
    @IBOutlet weak var langButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        userNameInputView.contentTextField.accessibilityIdentifier = "userName"
        passwordInputView.isSecret = false
        passwordInputView.contentTextField.accessibilityIdentifier = "password"
        
        if let _ =  ASUserDefaults.currentUser{
            showMainView()
        }

        viewModel.onLoggedInSubject.subscribe(onNext: {  [weak self] (result) in
            self?.showMainView()
            }).disposed(by: bag)
        passwordInputView.inputType = .password
        validator.registerInputView(userNameInputView, rules: [RequiredRule(message: "")])
        validator.registerInputView(passwordInputView, rules: [RequiredRule(message: "")])
        
        viewModel.onErrorSubject.subscribe(onNext: { (error) in
            self.hideDefaultLoader()
            self.showErrorWith(message: error)
        }).disposed(by: bag)
        
        
        if LocalizeService.shared.selectedLanguage == .arabic {
            langButton.setTitle("EN", for: .normal)
        } else {
            langButton.setTitle("AR", for: .normal)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
       @IBAction func didPressLogin(_ sender: Any) {
            validator.validate(self)
        }
    
    func showMainView() {
        self.hideDefaultLoader()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController
        self.show(vc ?? UIViewController(), sender: nil)
    }
        
}

    extension LoginViewController : ValidationDelegate {
        func validationSuccessful() {
            
            if userNameInputView.inputedValue() == "test" && userNameInputView.inputedValue() == "test" {
                let user = UserInfo()
                    user.SubName = "Mohammed"
                    user.Sub_Code = "-10"
                    user.Sub_ID = "-10"
                    ASUserDefaults.currentUser = user
                viewModel.onLoggedInSubject.onNext(true)
                return;
            }
            
            viewModel.loginParams.UName = userNameInputView.inputedValue()
            viewModel.loginParams.UPassword = passwordInputView.inputedValue()
            self.showDefaultLoader()
            
            viewModel.login()
        }
        
        func validationFailed(_ errors: [(Validatable, ValidationError)]) {
            
        }
        
        @IBAction func didPressChangeLange(_ sender: Any) {
            if LocalizeService.shared.selectedLanguage == .arabic {
                LocalizeService.shared.setLanguage(.english)
            } else {
                LocalizeService.shared.setLanguage(.arabic)
            }
            UIApplication.shared.didChangeLang()
            
        }
        
    }


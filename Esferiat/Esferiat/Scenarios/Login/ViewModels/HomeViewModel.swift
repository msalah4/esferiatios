//
//  HomeViewModel.swift
//  Esferiat
//
//  Created by Mohammed Salah on 19/08/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//

import UIKit
import RxSwift
class HomeViewModel: NSObject {

    var onErrorSubject = PublishSubject<String>()
    var onFetchUserInfoSubject = PublishSubject<UserDetailsHome>()
    var onFetchMonthlyQSubject = PublishSubject<Question>()
    var onFetchMonthlyQOptionsSubject = PublishSubject<[MCQuestion]>()
    var onSubmitMonthlyQSubject = PublishSubject<Bool>()
    var onDeleteAccountSubject = PublishSubject<Bool>()
    let bag = DisposeBag()
    
    
    func fetchHomeUserInfo()  {
        let api = QuestionsApi(params: nil)
        
        api.getUserInfo(id: ASUserDefaults.currentUser?.Sub_ID ?? "").subscribe(onNext: { [weak self](response) in
            self?.onFetchUserInfoSubject.onNext(response)
        }, onError: { [weak self] (error) in
            self?.onErrorSubject.onNext(error.message )
        }).disposed(by: bag)
    }
    
    func fetchMonthlyQuestion()  {
        let api = QuestionsApi(params: nil)
        
        api.getMonthQuestion(id: ASUserDefaults.currentUser?.Sub_ID ?? "").subscribe(onNext: { [weak self](response) in
            self?.onFetchMonthlyQSubject.onNext(response)
        }, onError: { [weak self] (error) in
            self?.onErrorSubject.onNext(error.message )
        }).disposed(by: bag)
    }
    
    func fetchMonthlyQuestionOptions(id: String?)  {
        let api = QuestionsApi(params: nil)
        api.qID = id ?? ""
        api.getMonthQuestionOptions().subscribe(onNext: { [weak self](response) in
            self?.onFetchMonthlyQOptionsSubject.onNext(response)
        }, onError: { [weak self] (error) in
            self?.onErrorSubject.onNext(error.message )
        }).disposed(by: bag)
    }
    
    func submitMonthlyQuestion(answer: String, qID: String)  {
        let api = QuestionsApi(params: nil)
        
        api.submitMonthQuestionAnswer(id: ASUserDefaults.currentUser?.Sub_ID ?? "", answer: answer, qID: qID).subscribe(onNext: { [weak self](response) in
            self?.onSubmitMonthlyQSubject.onNext(true)
        }, onError: { [weak self] (error) in
            self?.onErrorSubject.onNext(error.message )
        }).disposed(by: bag)
    }
    
    func deleteAccount()  {
        let api = QuestionsApi(params: nil)
        
        api.deleteAccount(email: ASUserDefaults.currentUser?.Sub_ID ?? "").subscribe(onNext: { [weak self](response) in
            self?.onDeleteAccountSubject.onNext(true)
        }, onError: { [weak self] (error) in
            self?.onErrorSubject.onNext(error.message )
        }).disposed(by: bag)
    }
    
    
}

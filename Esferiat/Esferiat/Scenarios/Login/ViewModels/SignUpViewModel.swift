//
//  SignUpViewModel.swift
//  Esferiat
//
//  Created by Mohammed Salah on 25/07/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//

import UIKit
import RxSwift
class SignUpViewModel: NSObject {
    
    
    var onErrorSubject = PublishSubject<String>()
    var onSignedUpSubject = PublishSubject<SignUpResponse>()
    var onLoggedInSubject = PublishSubject<Bool>()
    var params: SignupRequesrParams = SignupRequesrParams()
    var loginParams: LoginRequestParamters = LoginRequestParamters()
    let bag = DisposeBag()
    
    
    func signupNewUser() {
        let loginApi = LoginApi(params: params)
        
        loginApi.createAccount().subscribe(onNext: { (response) in
            if response.Code == "0" {
                self.onErrorSubject.onNext("This user registered before")
            } else {
                self.onSignedUpSubject.onNext(response)
            }
        }, onError: { (error) in
            self.onErrorSubject.onNext(error.message)
            }).disposed(by: bag)
    }
    
    func login() {
        let loginApi = LoginApi(params: loginParams)

        loginApi.login().subscribe(onNext: { (response) in
            ASUserDefaults.currentUser = response
            self.onLoggedInSubject.onNext(true)
        }, onError: { (error) in
            self.onErrorSubject.onNext(error.message)
            }).disposed(by: bag)
    }
}

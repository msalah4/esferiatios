//
//  Question.swift
//  Esferiat
//
//  Created by Mohammed Salah on 21/08/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//

import UIKit

class Question: Codable {

    var Q_ID: String?
    var Q_Description: String?
    var Q_Answer: String?
    var Q_DescriptionEn: String?
}

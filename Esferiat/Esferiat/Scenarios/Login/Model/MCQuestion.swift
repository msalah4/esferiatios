//
//  MCQuestion.swift
//  Esferiat
//
//  Created by Mohammed Salah on 28/11/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//

import UIKit

class MCQuestion: Codable {

    var QAnswerID: String?
    var Q_Choice: String?
    var Q_Right: String?
}

//
//  Partner.swift
//  Esferiat
//
//  Created by Mohammed Salah on 31/10/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//

import UIKit

class Partner: Codable {
    
    var P_ID: String?
    var P_Name: String?
    var P_Note: String?
    var P_Path: String?
    var Active: String?
}

//
//  Card.swift
//  Esferiat
//
//  Created by Mohammed Salah on 26/10/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//

import UIKit

class Card: Codable {
    
    var C_ID: String?
    var C_Name: String?
    var C_Path: String?
    var Active: String?

}

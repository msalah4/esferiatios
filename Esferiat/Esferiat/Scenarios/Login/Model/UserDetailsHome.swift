//
//  UserDetailsHome.swift
//  Esferiat
//
//  Created by Mohammed Salah on 19/08/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//

import UIKit

class UserDetailsHome: Codable {

    var Subscriptions: String?
    var TrueAnswers: String?
    var FalseAnswers: String?
}

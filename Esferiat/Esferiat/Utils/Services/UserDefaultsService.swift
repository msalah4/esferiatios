//
//  UserDefaultsService.swift
//  Customer IOS
//
//  Created by Michele on 11/19/19.
//  Copyright © 2019 Michelle. All rights reserved.
//

import UIKit

class UserDefaultsService: NSObject {
    
    private enum UserDefaultsKey: String {
        case accessToken
        case firebase_token
    }
    
    static var shared: UserDefaultsService = {
        return UserDefaultsService()
    }()
    
    func clearUserData() {
        UserDefaults.standard.removeObject(forKey: UserDefaultsKey.accessToken.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    // MARK: - access token
    var isUserLoggedin: Bool {
        UserDefaults.standard.object(forKey: UserDefaultsKey.accessToken.rawValue) != nil
    }
    
    func saveAccessToken (_ token: String?) {
        UserDefaults.standard.set(token, forKey: UserDefaultsKey.accessToken.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    var accessToken: String? {
        UserDefaults.standard.object(forKey: UserDefaultsKey.accessToken.rawValue) as? String
    }
    
    static var firebase_token: String {
        get {
            return  UserDefaults.standard.string(forKey: UserDefaultsKey.accessToken.rawValue) ?? ""
        }
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: UserDefaultsKey.accessToken.rawValue)
        }
    }
}

//
//  File.swift
//  Localize
//
//  Created by Michele on 9/9/19.
//  Copyright © 2019 Michelle. All rights reserved.
//

import Foundation
import UIKit
import L10n_swift

class LocalizeService {
    
    enum Langaue: String {
        case english = "en"
        case arabic = "ar"
        
        var locale: String {
            switch self {
            case .english:
                return "en-US"
            case .arabic:
                return "ar-SA"
            }
        }
        
        var title: String {
            switch self {
            case .english:
                return "English"
            case .arabic:
                return "العربية"
            }
        }
        
        var apiHeaderValue: String {
            switch self {
            case .english:
                return "en"
            case .arabic:
                return "ar"
            }
        }
    }
    
    private let selectedLanguageKey = "selectedLangaue"
    
    static var shared: LocalizeService = {
        return LocalizeService()
    }()
    
    private (set) var deviceLanguage: Langaue
    var selectedLanguage: LocalizeService.Langaue
    
    private init() {
        
        let deviceLanguageString = Bundle.main.preferredLocalizations.first ?? "ar"
        if deviceLanguageString.contains(Langaue.arabic.rawValue) {
            self.deviceLanguage = LocalizeService.Langaue.arabic
        } else {
            self.deviceLanguage = LocalizeService.Langaue.english
        }
        
        if let userdefaultsValue = UserDefaults.standard.string(forKey: selectedLanguageKey),
            let preselectedLanguage = LocalizeService.Langaue(rawValue: userdefaultsValue) {
            self.selectedLanguage = preselectedLanguage
        } else {
            self.selectedLanguage = self.deviceLanguage
        }
    }
    
    var isRTL: Bool {
        return self.isArabic
    }
    
    var isArabic: Bool {
        return self.selectedLanguage == LocalizeService.Langaue.arabic
    }
    
    var isTheSameDeviceLanguage: Bool {
        return self.deviceLanguage == self.selectedLanguage
    }
    
    func startService() {
        self.changeLocalizedStringImplementaion()
        self.setLanguage(self.selectedLanguage)
    }
    
    func setLanguage(_ language: Langaue) {
        
        self.selectedLanguage = language
        UserDefaults.standard.setValue(self.selectedLanguage.rawValue, forKey: selectedLanguageKey)
        if self.isRTL {
            UIView.appearance().direction = .forceRightToLeft
            UIView.appearance(whenContainedInInstancesOf: [ReversedDirectionView.self]).direction = .forceLeftToRight
        } else {
            UIView.appearance().direction = .forceLeftToRight
            UIView.appearance(whenContainedInInstancesOf: [ReversedDirectionView.self]).direction = .forceRightToLeft
        }
        UIView.appearance(whenContainedInInstancesOf: [LeftDirectionView.self]).direction = .forceLeftToRight
        UIView.appearance(whenContainedInInstancesOf: [RightDirectionView.self]).direction = .forceRightToLeft
    }
    
    private func changeLocalizedStringImplementaion() {
        
        let nativeSelector = #selector(Bundle.localizedString(forKey:value:table:))
        let customSelector = #selector(Bundle.customLocalizedString(forKey:value:table:))
        guard let nativeMethod: Method = class_getInstanceMethod(Bundle.self, nativeSelector),
            let customMethod: Method = class_getInstanceMethod(Bundle.self, customSelector) else {
                fatalError("Error with 'LocalizedString' method_exchangeImplementations")
        }
        method_exchangeImplementations(nativeMethod, customMethod)
    }
}

extension Bundle {
    @objc
    fileprivate func customLocalizedString(forKey key: String, value: String?, table tableName: String?) -> String {
        var associatedLanguageBundle:Character = "0"
        let bundleNow: Bundle? = objc_getAssociatedObject(self, &associatedLanguageBundle) as? Bundle
        var extraB = Bundle(path: Bundle.main.path(forResource: L10n.shared.language, ofType: "lproj") ?? "")
        if extraB ==  nil {
            extraB = Bundle(path: Bundle.main.path(forResource: "Base", ofType: "lproj") ?? "")
        }
        return (bundleNow != nil) ? (bundleNow!.customLocalizedString(forKey: key, value: value, table: tableName)) : (extraB!.customLocalizedString(forKey: key, value: value, table: tableName))


        
        var bundle = self
        
        if self == Bundle.main &&
            !LocalizeService.shared.isTheSameDeviceLanguage {
            
            let selectedLanguage = LocalizeService.shared.selectedLanguage.rawValue
            guard let bundlePath = Bundle.main.path(forResource: selectedLanguage, ofType: "lproj") else {
                fatalError("No Bundle For Selected Language")
            }
            bundle = Bundle(path: bundlePath) ?? self
        }
        
        return bundle.customLocalizedString(forKey: key, value: value, table: tableName)
    }
}

class ReversedDirectionView: UIView {
    // left alignment with arabic
    // right alignment with english
}

class LeftDirectionView: UIView {
    // left alignment with arabic and english
}

class RightDirectionView: UIView {
    // right alignment with arabic and english
}

extension UIView {
    @objc var direction: UISemanticContentAttribute {
        get { return self.semanticContentAttribute }
        set(direction) {
            
            if self.semanticContentAttribute == direction { return }
            
            self.semanticContentAttribute = direction
            (self as? UILabel)?.direction = direction
            (self as? UIButton)?.direction = direction
            (self as? UITextField)?.direction = direction
            (self as? UITextView)?.direction = direction
        }
    }
}

extension UILabel {
    @objc override var direction: UISemanticContentAttribute {
        get { return self.semanticContentAttribute }
        set(direction) {
            if direction == .forceRightToLeft && self.textAlignment == .left {
                self.textAlignment = .right
            } else if direction == .forceLeftToRight && self.textAlignment == .right {
                self.textAlignment = .left
            }
        }
    }
}

extension UIButton {
    @objc override var direction: UISemanticContentAttribute {
        get { return self.semanticContentAttribute }
        set(direction) {
            if direction == .forceRightToLeft && self.contentHorizontalAlignment == .left {
                self.contentHorizontalAlignment = .right
            } else if direction == .forceLeftToRight && self.contentHorizontalAlignment == .right {
                self.contentHorizontalAlignment = .left
            }
        }
    }
    
    @IBInspectable var isFlippable: Bool {
        get { return false }
        set(isFlippable) {
            self.imageView?.isFlippable = isFlippable
        }
    }
}

extension UITextView {
    @objc override var direction: UISemanticContentAttribute {
        get { return self.semanticContentAttribute }
        set(direction) {
            if direction == .forceRightToLeft && self.textAlignment == .left {
                self.textAlignment = .right
            } else if direction == .forceLeftToRight && self.textAlignment == .right {
                self.textAlignment = .left
            }
        }
    }
}

extension UITextField {
    @objc override var direction: UISemanticContentAttribute {
        get { return self.semanticContentAttribute }
        set(direction) {
            if direction == .forceRightToLeft && self.textAlignment == .left {
                self.textAlignment = .right
            } else if direction == .forceLeftToRight && self.textAlignment == .right {
                self.textAlignment = .left
            }
        }
    }
}

extension UIImageView {
    @IBInspectable var isFlippable: Bool {
        get { return false }
        set(isFlippable) {
            if LocalizeService.shared.isRTL && isFlippable {
                if self.transform.a == 1 {
                    self.transform = CGAffineTransform(scaleX: -1, y: 1)
                } else {
                    self.transform = CGAffineTransform(scaleX: 1, y: 1)
                }
            }
        }
    }
}

extension UICollectionViewFlowLayout {
    open override var flipsHorizontallyInOppositeLayoutDirection: Bool {
        return true
    }
}

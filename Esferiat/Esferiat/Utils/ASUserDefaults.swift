//
//  UserDefaults.swift
//  Esferiat
//
//  Created by Mohammed Salah on 17/08/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//

import UIKit
import Default

class ASUserDefaults: NSObject {

    fileprivate static let LoggedInUserKey = "LoggedInUser"
    
    static var currentUser: UserInfo? {
        set {
            if let data = try? JSONEncoder().encode(newValue) {
                UserDefaults.standard.set(data, forKey: LoggedInUserKey)
            } else {
                UserDefaults.standard.set(nil, forKey: LoggedInUserKey)
            }
        }
        get {
            if let data = UserDefaults.standard.value(forKey: LoggedInUserKey) as? Data{
                return try? JSONDecoder().decode(UserInfo.self, from: data)
            } else {
                return nil
            }
        }
    }

}

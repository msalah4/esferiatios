//
//  AppConfigurations.swift
//  Coach You
//
//  Created by Mohammed Salah on 05/05/2020.
//  Copyright © 2020 msalah. All rights reserved.
//

import UIKit

enum ProjectMode: String {
    case development
    case stage
    case production
}

class AppConfigurations: NSObject {

    static var mode: ProjectMode = .development
    
    static var BaseUrl: URL {
        if AppConfigurations.mode == .development {
            return URL(string: "https://mutaweronteam.com/")!
        } else if AppConfigurations.mode == .production {
            return URL(string: "https://mutaweronteam.com/")!
        } else {
            return URL(string: "https://mutaweronteam.com/")!
        }
    }
    
}

//
//  BaseViewController.swift
//  Coach You
//
//  Created by Mohammed Salah on 05/05/2020.
//  Copyright © 2020 msalah. All rights reserved.
//

import UIKit
import RxSwift
import NVActivityIndicatorView
import AlamofireImage

class BaseViewController: UIViewController {
    
    var activityIndicatorView: NVActivityIndicatorView?
    var viewLoadingContainer: UIView?
    var bag = DisposeBag()
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var userButton: UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    @IBAction func didPressBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    func showDefaultLoader(backgroundColor: UIColor = UIColor.darkGray.withAlphaComponent(0.5)) {
        var barHeight = (navigationController?.navigationBar.frame.height ?? 44) + UIApplication.shared.statusBarFrame.height
        if navigationController?.navigationBar.isHidden ?? false || navigationController?.navigationBar == nil {
            barHeight = 0
        }
        if activityIndicatorView == nil {
            activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50), type: .circleStrokeSpin, color: .mutedPurple, padding: 0)
        }

        var center = CGPoint()
        if let frame = navigationController?.view.bounds {
            viewLoadingContainer = UIView(frame: frame)
            viewLoadingContainer?.frame.origin.y = barHeight
            center = (navigationController?.view.center)!
        } else if let windowFrame = UIApplication.shared.keyWindow?.frame {
            viewLoadingContainer = UIView(frame: windowFrame)
            center = UIApplication.shared.keyWindow!.center
            viewLoadingContainer?.frame.origin.y = barHeight
        }
        center.y -= barHeight
        activityIndicatorView?.center = center
        viewLoadingContainer?.backgroundColor = backgroundColor
        activityIndicatorView?.startAnimating()
        viewLoadingContainer?.addSubview(activityIndicatorView!)

        view.addSubview(viewLoadingContainer!)
    }
    
    func hideDefaultLoader() {
        if let activity = activityIndicatorView {
            activity.removeFromSuperview()
            activity.stopAnimating()
        }

        if let loadingContainer = viewLoadingContainer {
            loadingContainer.removeFromSuperview()
        }
    }
    
    func showErrorWith( message: String, title: String = "خطاء") {
        AlertView(title: title, message: message).withOneOption(title: "حسنا").show(view: self)
    }

}

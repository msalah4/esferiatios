//
//  BaseTabsViewControllerViewController.swift
//  Coach You
//
//  Created by Mohammed Salah on 09/05/2020.
//  Copyright © 2020 msalah. All rights reserved.
//

import UIKit

class BaseTabsViewController: UIViewController {

    var viewControllers: [UIViewController]!
    @IBOutlet weak var contentView: UIView!
    var currentTab = 0
    var firstViewRealWidth = 0
    var secondViewRealWidth = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        viewControllers.map({self.addChild($0)})
        contentView.addSubview(viewControllers.first?.view ?? UIView())
        currentTab = 1
    }
    
    @IBAction func didPressFirstTab(_ sender: UIButton) {
        if currentTab == 1 {
            return
        }
//        contentView.removes
        currentTab = 1
        
    }
    
    @IBAction func didPressSecondTab(_ sender: UIButton) {
        if currentTab == 2 {
            return
        }
    }
    

}

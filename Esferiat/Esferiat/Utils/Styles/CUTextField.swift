//
//  CUTextField.swift
//  Coach You
//
//  Created by Mohammed Salah on 08/05/2020.
//  Copyright © 2020 msalah. All rights reserved.
//

import UIKit

class CUTextField: UITextField {
    
    let textPadding = UIEdgeInsets.init(top: 0, left: 12, bottom: 0, right: 12)

    override func awakeFromNib() {
        
    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: textPadding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: textPadding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: textPadding)
    }
}

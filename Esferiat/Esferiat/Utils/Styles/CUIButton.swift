//
//  CUIButton.swift
//  Coach You
//
//  Created by Mohammed Salah on 05/05/2020.
//  Copyright © 2020 msalah. All rights reserved.
//

import UIKit

enum UIButtonStyles:Int {
    case none
    case purpleWithRounded
    case grayWithRounded
}

class CUIButton: UIButton {
    
    @IBInspectable
    var theme: Int = UIButtonStyles.none.rawValue {
        didSet{
            applyTheme()
        }
    }
}


extension CUIButton {
    
    func applyTheme() {
        switch theme {
        case UIViewStyles.none.rawValue:
            break
        case UIButtonStyles.purpleWithRounded.rawValue:
            self.cornerRadius = 8
            self.backgroundColor = UIColor(named: "mutedPurple")
        case UIButtonStyles.grayWithRounded.rawValue:
            self.cornerRadius = 15
            self.backgroundColor = UIColor(named: "iceBlue")
            self.setTitleColor(UIColor(named: "charcoalGrey"), for: .normal)
        default:
            break
        }
    }
}

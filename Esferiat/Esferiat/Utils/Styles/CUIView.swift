//
//  UIView+Style.swift
//  Coach You
//
//  Created by Mohammed Salah on 05/05/2020.
//  Copyright © 2020 msalah. All rights reserved.
//

import UIKit

enum UIViewStyles:Int {
    case none
    case mainThemePickerWithRounded
    case dashedView
}

class CUIView: UIView {
    
    @IBInspectable
    var theme: Int = UIViewStyles.none.rawValue
//    {
//        didSet{
//
//        }
//    }


 override func awakeFromNib() {
     super.awakeFromNib()
     
 }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        applyTheme()
    }

}
extension CUIView {
    
    func applyTheme() {
        switch theme {
        case UIViewStyles.none.rawValue:
            break
        case UIViewStyles.mainThemePickerWithRounded.rawValue:
            self.layer.cornerRadius = 25
            self.roundedCornerSide = "topLeft,topRight"
            self.backgroundColor = UIColor(named: "mutedPurple")
        case UIViewStyles.dashedView.rawValue:
            self.clipsToBounds = false
            self.layer.masksToBounds = true
            let dottedLayer: CALayer = dashedBorderLayerWithColor(color: UIColor.brownGrey.cgColor, view: self)
            self.layer.addSublayer(dottedLayer)
        default:
            break
        }
    }
}

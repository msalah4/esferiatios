//
//  UITableView+Extension.swift
//  Coach You
//
//  Created by Mohammed Salah on 20/05/2020.
//  Copyright © 2020 msalah. All rights reserved.
//

import UIKit

extension UITableView {
    
    func removeFooter() {
        self.tableFooterView = UIView(frame: CGRect.zero)
    }
}

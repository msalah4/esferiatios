//
//  Bundle+Extension.swift
//  Coach You
//
//  Created by Mohammed Salah on 05/05/2020.
//  Copyright © 2020 msalah. All rights reserved.
//

import UIKit


private var associatedLanguageBundle:Character = "0"

class PrivateBundle: Bundle {
    override func localizedString(forKey key: String, value: String?, table tableName: String?) -> String {
        let bundle: Bundle? = objc_getAssociatedObject(self, &associatedLanguageBundle) as? Bundle
        return (bundle != nil) ? (bundle!.localizedString(forKey: key, value: value, table: tableName)) : (super.localizedString(forKey: key, value: value, table: tableName))

    }
}

extension Bundle {
    class func setLanguage(_ language: String) {
        var onceToken: Int = 0

        if (onceToken == 0) {
            /* TODO: move below code to a static variable initializer (dispatch_once is deprecated) */
            object_setClass(Bundle.main, PrivateBundle.self)
        }
        onceToken = 1
        objc_setAssociatedObject(Bundle.main, &associatedLanguageBundle, Bundle(path: Bundle.main.path(forResource: language, ofType: "lproj") ?? ""), .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        
        let isRTL  = (Locale.characterDirection(forLanguage: language) == .rightToLeft)
        if isRTL {
            
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            UITextField.appearance().semanticContentAttribute = .forceRightToLeft
        } else {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            UITextField.appearance().semanticContentAttribute = .forceLeftToRight
        }
        UserDefaults.standard.set(language, forKey: "currentLanguageKey")
//        UserDefaults.standard.set([L10n.shared.language], forKey: "AppleLanguages")
        UserDefaults.standard.set(isRTL, forKey: "AppleTextDirection")
        UserDefaults.standard.set(isRTL, forKey: "NSForceRightToLeftWritingDirection")
        UserDefaults.standard.synchronize()
    }
}

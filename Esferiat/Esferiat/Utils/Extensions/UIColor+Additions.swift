//
//  UIColor+Additions.swift
//  Coach You
//
//  Generated on Zeplin. (05/05/2020).
//  Copyright (c) 2020 __MyCompanyName__. All rights reserved.
//

import UIKit

extension UIColor {

  @nonobjc class var iceBlue: UIColor {
    return UIColor(red: 238.0 / 255.0, green: 241.0 / 255.0, blue: 242.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var mutedPurple: UIColor {
    return UIColor(red: 116.0 / 255.0, green: 84.0 / 255.0, blue: 150.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var white: UIColor {
    return UIColor(white: 1.0, alpha: 1.0)
  }

  @nonobjc class var charcoalGrey: UIColor {
    return UIColor(red: 54.0 / 255.0, green: 54.0 / 255.0, blue: 56.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var mutedPurpleTwo: UIColor {
    return UIColor(red: 115.0 / 255.0, green: 83.0 / 255.0, blue: 150.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var paleLavender: UIColor {
    return UIColor(red: 239.0 / 255.0, green: 225.0 / 255.0, blue: 1.0, alpha: 1.0)
  }

  @nonobjc class var darkGreyBlue: UIColor {
    return UIColor(red: 52.0 / 255.0, green: 67.0 / 255.0, blue: 86.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var dark70: UIColor {
    return UIColor(red: 20.0 / 255.0, green: 15.0 / 255.0, blue: 38.0 / 255.0, alpha: 0.7)
  }

  @nonobjc class var paleGrey: UIColor {
    return UIColor(red: 236.0 / 255.0, green: 238.0 / 255.0, blue: 244.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var veryLightPink: UIColor {
    return UIColor(white: 216.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var blueyGrey: UIColor {
    return UIColor(red: 163.0 / 255.0, green: 163.0 / 255.0, blue: 164.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var flatBlue: UIColor {
    return UIColor(red: 63.0 / 255.0, green: 125.0 / 255.0, blue: 183.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var scarlet: UIColor {
    return UIColor(red: 208.0 / 255.0, green: 2.0 / 255.0, blue: 27.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var paleGreyTwo: UIColor {
    return UIColor(red: 244.0 / 255.0, green: 245.0 / 255.0, blue: 249.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var brownGrey: UIColor {
    return UIColor(white: 155.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var cloudyBlue: UIColor {
    return UIColor(red: 194.0 / 255.0, green: 196.0 / 255.0, blue: 202.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var bluishGrey: UIColor {
    return UIColor(red: 128.0 / 255.0, green: 139.0 / 255.0, blue: 140.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var dark: UIColor {
    return UIColor(red: 36.0 / 255.0, green: 54.0 / 255.0, blue: 64.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var veryLightPinkTwo: UIColor {
    return UIColor(white: 242.0 / 255.0, alpha: 1.0)
  }

  @nonobjc class var tealBlue: UIColor {
    return UIColor(red: 0.0, green: 168.0 / 255.0, blue: 157.0 / 255.0, alpha: 0.25)
  }
    
  @nonobjc class var black: UIColor {
    return UIColor(white: 51.0 / 255.0, alpha: 1.0)
  }
  
  @nonobjc class var blueyGreyTwo: UIColor {
    return UIColor(red: 152.0 / 255.0, green: 157.0 / 255.0, blue: 179.0 / 255.0, alpha: 1.0)
  }

    @nonobjc class var darkBlueGrey: UIColor {
      return UIColor(red: 31.0 / 255.0, green: 49.0 / 255.0, blue: 74.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var bluishGreen: UIColor {
      return UIColor(red: 14.0 / 255.0, green: 167.0 / 255.0, blue: 72.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var trueGreen: UIColor {
      return UIColor(red: 0.0, green: 162.0 / 255.0, blue: 12.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var apple: UIColor {
        return UIColor(red: 109.0 / 255.0, green: 201.0 / 255.0, blue: 48.0 / 255.0, alpha: 1.0)
    }
    
    
    @nonobjc class var mango: UIColor {
        return UIColor(red: 255.0 / 255.0, green: 171.0 / 255.0, blue: 43.0 / 255.0, alpha: 1.0)
    }
    

    @nonobjc class var darkGrey: UIColor {
      return UIColor(red: 36.0 / 255.0, green: 37.0 / 255.0, blue: 41.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var blueGrey: UIColor {
      return UIColor(red: 132.0 / 255.0, green: 138.0 / 255.0, blue: 148.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var lightBlueGrey: UIColor {
      return UIColor(red: 197.0 / 255.0, green: 198.0 / 255.0, blue: 207.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var purpleyGrey: UIColor {
      return UIColor(red: 140.0 / 255.0, green: 136.0 / 255.0, blue: 150.0 / 255.0, alpha: 1.0)
    }
    
    @nonobjc class var whitetag: UIColor {
      return UIColor(white: 247.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var darkSlateBlue5: UIColor {
      return UIColor(red: 20.0 / 255.0, green: 40.0 / 255.0, blue: 80.0 / 255.0, alpha: 0.05)
    }

    @nonobjc class var lightPeriwinkle: UIColor {
      return UIColor(red: 204.0 / 255.0, green: 204.0 / 255.0, blue: 227.0 / 255.0, alpha: 1.0)
    }

    @nonobjc class var blackTwo: UIColor {
      return UIColor(white: 55.0 / 255.0, alpha: 1.0)
    }
}

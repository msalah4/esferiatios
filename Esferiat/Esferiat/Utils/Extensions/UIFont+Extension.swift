//
//  UIFont+Extension.swift
//  Coach You
//
//  Created by Mohammed Salah on 05/05/2020.
//  Copyright © 2020 msalah. All rights reserved.
//

import UIKit

extension UIFont {

  class var textStyle: UIFont {
    return UIFont(name: "Poppins-Bold", size: 30.0)!
  }

  class var textStyle4: UIFont {
    return UIFont(name: "Poppins-Regular", size: 24.0)!
  }

  class var textStyle3: UIFont {
    return UIFont(name: "GESSTextLight-Light", size: 24.0)!
  }

  class var textStyle5: UIFont {
    return UIFont(name: "Poppins-Regular", size: 16.0)!
  }

  class var textStyle2: UIFont {
    return UIFont(name: "Poppins-Regular", size: 16.0)!
  }
    
  class var textStyle14: UIFont {
      return UIFont(name: "Poppins-Regular", size: 14.0)!
  }

}

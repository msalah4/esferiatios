//
//  UIApplication+Ex.swift
//  Esferiat
//
//  Created by Mohammed Salah on 27/11/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//

import UIKit
import L10n_swift
extension UIApplication {
    
    var topViewController: UIViewController? {
        
        let window = (self.delegate as? AppDelegate)?.window
        let rootNaviationController = window?.rootViewController as? UINavigationController
        return rootNaviationController?.topViewController ?? window?.rootViewController
    }
    
    var window: UIWindow? {
        return (self.delegate as? AppDelegate)?.window
    }
    
    func restartTo(_ viewController: UIViewController) {
        
        if let window = self.window {
            
            UIView.transition(with: window,
                              duration: 0.5,
                              options: .transitionCrossDissolve,
                              animations: {
                                window.rootViewController = viewController
                                
            },
                              completion: nil)
        }
    }
    
    func didChangeLang () {
        if LocalizeService.shared.selectedLanguage == .arabic {
            L10n.shared.language = "en"
        } else {
            L10n.shared.language = "ar"
        }
        let storybaord = UIStoryboard.init(name: "Login", bundle: nil)
        let baseView = storybaord.instantiateViewController(withIdentifier: "start") as! UINavigationController
        if let _ =  ASUserDefaults.currentUser{
//            let vc = storybaord.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController
//            baseView.pushViewController(vc!, animated: false)
        }
        
        UIApplication.shared.restartTo(baseView)
    }
}

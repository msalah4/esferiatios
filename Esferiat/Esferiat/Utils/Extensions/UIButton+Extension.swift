//
//  UIButton+Extension.swift
//  Coach You
//
//  Created by Mohammed Salah on 17/05/2020.
//  Copyright © 2020 msalah. All rights reserved.
//

import UIKit


extension UIButton {
    func makeTabButtonSelected() {
        self.backgroundColor = .mutedPurple
        self.setTitleColor(.white, for: .normal)
    }
    
    func makeTabButtonUnselected() {
       self.backgroundColor = .iceBlue
       self.setTitleColor(.cloudyBlue, for: .normal)
    }
}

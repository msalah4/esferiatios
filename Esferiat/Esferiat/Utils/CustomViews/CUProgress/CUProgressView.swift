//
//  CUProgressView.swift
//  Coach You
//
//  Created by Mohammed Salah on 07/05/2020.
//  Copyright © 2020 msalah. All rights reserved.
//

import UIKit

class CUProgressView: UIView {
    
    let barHeight = CGFloat( 5.0)
    let imageSize = 19.0
    let labelSize = 20.0
    let barRadius = CGFloat(3)
    
    var imageView:UIImageView!
    var filledView:UIView!
    var barView:UIView!
    var label:UILabel!
    
    @IBInspectable
    var steps:Int = 2
    
    @IBInspectable
    var currentSteps:Int = 1 {
        didSet {
            updateSteps()
        }
    }
    
    override func draw(_ rect: CGRect) {
        updateSteps()
    }
    
    func updateSteps () {
        
        if barView == nil {
            barView = UIView(frame: self.bounds )//CGRect(x: 0, y: 0, width: self.frame.size.width, height: barHeight))
            var frame = barView.frame
            frame.size.height = barHeight
            barView.backgroundColor = .paleGreyTwo
            barView.frame = frame
            barView.cornerRadius = barRadius
            self.addSubview(barView)
        }
        
        if filledView == nil {
            filledView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: barHeight))
            filledView.backgroundColor = .flatBlue
            filledView.cornerRadius = barRadius
            barView.addSubview(filledView)
        }
        
        if imageView == nil {
            imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: imageSize, height: imageSize))
            imageView.center = barView.center
            imageView.image = UIImage(named: "selected")
            self.addSubview(imageView)
        }
        
        if label == nil {
            label = UILabel(frame: CGRect(x: Double(self.frame.size.width) - labelSize - 10, y: 9, width: labelSize + 10, height: labelSize))
            label.font = UIFont.systemFont(ofSize: 14.0)
            label.textColor = .flatBlue
            label.text = "\(currentSteps)/\(steps)"
            self.addSubview(label)
        }
        
        var frame = barView.frame
        frame.size.width = self.frame.size.width
        barView.frame = frame
        
        var labelFrame = label.frame
        labelFrame.origin.x = CGFloat(self.frame.size.width) - CGFloat(labelSize) - 10
        label.frame = labelFrame
        
        
        let totalWidth = self.frame.size.width
        var currentPoint = CGFloat(currentSteps) / CGFloat(steps) * totalWidth
        
        var filledViewFrame = filledView.frame
        filledViewFrame.size.width = currentPoint + 5
        
        var imageFrame = imageView.frame
        if currentSteps == steps {
            currentPoint = currentPoint - CGFloat(imageSize)
        }
        imageFrame.origin.x = currentPoint
        imageFrame.size.height = CGFloat(imageSize)
        
        UIView.animate(withDuration: 0.3, animations: {
            self.imageView.frame = imageFrame
            self.filledView.frame = filledViewFrame
        })
        
        label.text = "\(currentSteps)/\(steps)"
    }
    

}

//
//  CAlertView.swift
//  Coach You
//
//  Created by Mohammed Salah on 06/05/2020.
//  Copyright © 2020 msalah. All rights reserved.
//

import UIKit

@objc class AlertView: NSCoder {
    fileprivate var viewController:CAlertView!
    var onYes:() -> Void = {}
    var onNo:(() -> Void) = {}
    var onDismiss:() -> Void = {}
    
    init(title: String, message: String) {
       super.init()
        viewController = (Bundle.main.loadNibNamed("CAlertView", owner: nil, options: nil)?.first as? CAlertView)
        viewController.alertTitle.text = title
        viewController.message.text = message
   }
    
    func withOneOption(title: String) -> AlertView {
        viewController.setAlertWithOneOption()
        viewController.confirmButton.setTitle(title, for: .normal)
        return self
    }

    func withTwoOptions(confirmationTitle: String, cancelTitle: String) -> AlertView {
        viewController.confirmButton.setTitle(confirmationTitle, for: .normal)

        viewController.cancelButton.setTitle(cancelTitle, for: .normal)
        return self
    }

    func setConfirmartionCallback(OnYes: @escaping () -> Void) -> AlertView {
        viewController.onYes = OnYes
        return self
    }

    func setCancelCallback(OnNo: @escaping () -> Void) -> AlertView {
           viewController.onNo = OnNo
        return self
    }

    func onDismissPressed(OnDissmiss: @escaping () -> Void) -> AlertView {
        viewController.onDismiss = OnDissmiss
        return self
    }
    
    func show(view:UIViewController)  {
        viewController.modalPresentationStyle = .overFullScreen
        view.present(viewController, animated: true, completion: nil)
    }
    
    
    
    
}

 class CAlertView: UIViewController {
    
    @IBOutlet weak var alertTitle: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var confirmationConstraint: NSLayoutConstraint!
    @IBOutlet weak var confirmationConstraintFullWidth: NSLayoutConstraint!
    
    var onYes:() -> Void = {}
    var onNo:(() -> Void) = {}
    var onDismiss:() -> Void = {}

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func setAlertWithOneOption() {

        confirmationConstraintFullWidth.isActive = true
        confirmationConstraintFullWidth.priority = UILayoutPriority(rawValue: 1000)
        confirmationConstraint.isActive = false
        confirmButton.layoutIfNeeded()

        cancelButton.isHidden = true
    }

    @IBAction func didPressConfirm(_ sender: UIButton) {
        onYes()
        self.dismiss(animated: true, completion: nil)
        onDismiss()
    }
    
    @IBAction func didPressCancel(_ sender: UIButton) {
        onNo()
        self.dismiss(animated: true, completion: nil)
        onDismiss()
    }
    
    
}

extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem!, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}

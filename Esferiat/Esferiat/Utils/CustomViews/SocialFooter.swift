//
//  SocialFooter.swift
//  Esferiat
//
//  Created by Mohammed Salah on 20/10/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//

import UIKit

class SocialFooter: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var fbButton: UIButton!
    var twButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let center = self.frame.width / 2.0
        let size = frame.size.height
        
        if fbButton == nil {
            fbButton = UIButton(frame: CGRect(x: center - (size + 10), y: 0, width: size, height: size))
            fbButton.setImage(UIImage(named: "fb"), for: .normal)
            fbButton.addTarget(self, action: #selector(didPressFB(_:)), for: .touchUpInside)
            self.addSubview(fbButton)
        }
        
        if twButton == nil {
            twButton = UIButton(frame: CGRect(x: center + 10, y: 0, width: size, height: size))
            twButton.setImage(UIImage(named: "tw"), for: .normal)
            twButton.addTarget(self, action: #selector(didPressTW(_:)), for: .touchUpInside)
            self.addSubview(twButton)
        }
        
    }

    
    @objc @IBAction func didPressFB(_ sender: Any) {
        if let url = URL(string: "https://www.facebook.com/Esferiat") {
            UIApplication.shared.open(url)
        }
    }
    
    @objc @IBAction func didPressTW(_ sender: Any) {
        if let url = URL(string: "https://twitter.com/Esferiat") {
            UIApplication.shared.open(url)
        }
    }
}

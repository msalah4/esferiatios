//
//  PopupViewController.swift
//  Coach You
//
//  Created by Mohammed Salah on 17/05/2020.
//  Copyright © 2020 msalah. All rights reserved.
//

import UIKit

class PopupViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var doneBtn: UIButton!
    
    var onFetchedData: (([String]) -> Void)?
    var didSelectItemWithIndex: (([Int]) -> Void)?
    var listOfItems: [String] = []
    var isMultiSelection = false
    var selectedIndexs = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        showDefaultLoader()
        
        onFetchedData = { result in
            DispatchQueue.main.async {
                self.hideDefaultLoader()
                self.listOfItems = result
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        doneBtn.isHidden = !isMultiSelection
        tableView.allowsMultipleSelection = isMultiSelection
    }
    
    
    @IBAction func didPressCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didPressDone(_ sender: Any) {
        if let update = didSelectItemWithIndex {
            update(selectedIndexs)
            self.dismiss(animated: true, completion: nil)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    static func initiatePopup() -> PopupViewController? {
        let vc = (Bundle.main.loadNibNamed("PopupViewController", owner: nil, options: nil)?.first as? PopupViewController)
        vc?.modalPresentationStyle = .overFullScreen
        return vc
    }
}

extension PopupViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.font = UIFont(name: "Poppins-Regular", size: 14.0)
        cell.textLabel?.text = listOfItems[indexPath.row]
        
        if indexPath.row == listOfItems.count - 1 {
            cell.cornerRadius = 8
            cell.roundedCornerSide = "bottomLeft,bottomRight"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isMultiSelection {
            selectedIndexs.append(indexPath.row)
        } else {
            if let update = didSelectItemWithIndex {
                update([indexPath.row])
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if selectedIndexs.count > indexPath.row {
            let value = selectedIndexs[indexPath.row]
            selectedIndexs.removeAll(where: {$0 == value})
        }
    }
    
}

//
//  CUILable.swift
//  Coach You
//
//  Created by Mohammed Salah on 12/05/2020.
//  Copyright © 2020 msalah. All rights reserved.
//

import UIKit

class UnderlinedLabelView: UIView {
    
    @IBInspectable
    var titleText: String = ""
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var lineView: UIView!
    var contentView: UnderlinedLabel?
    
    required init?(coder aDecoder: NSCoder) {
         super.init(coder: aDecoder)

         guard let view = loadViewFromNib() else { return }
         view.frame = self.bounds
         self.addSubview(view)
         contentView = view
         self.titleLabel = contentView!.titleLabel
        self.lineView = contentView?.lineView
         
     }
     
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLabel.text = titleText
    }

     func loadViewFromNib() -> UnderlinedLabel? {
         let bundle = Bundle(for: type(of: self))
         let nib = UINib(nibName: "UnderLinedLabel", bundle: nil)
         return nib.instantiate(withOwner: self, options: nil).first as? UnderlinedLabel
     }
    
}


class UnderlinedLabel: UIView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var lineView: UIView!
    
    var text: String? = nil
    
    override func awakeFromNib() {
        
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        if let te = text {
            titleLabel.text = te
        }
    }

}

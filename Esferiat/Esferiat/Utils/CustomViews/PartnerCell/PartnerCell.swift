//
//  PartnerCell.swift
//  Esferiat
//
//  Created by Mohammed Salah on 31/10/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//

import UIKit

class PartnerCell: UITableViewCell {

    @IBOutlet weak var partnerImage: UIImageView!
    @IBOutlet weak var partnerDesc: UILabel!
    @IBOutlet weak var partnerName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

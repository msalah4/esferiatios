//
//  ProfileTypeView.swift
//  Coach You
//
//  Created by Mohammed Salah on 05/05/2020.
//  Copyright © 2020 msalah. All rights reserved.
//

import UIKit

protocol ProfileTypeViewDelegate:AnyObject {
    func didSelectProfile(profile: ProfileTypeView)
}

class ProfileTypeView: UIView {
    
    var isSelectedProfile: Bool = false {
        didSet{
            if isSelectedProfile {
                profileTypeTitle.textColor = .mutedPurpleTwo
                profileTypeImage.borderColor = .mutedPurpleTwo
                profileTypeImage.borderWidth = 3.0
            } else {
                profileTypeTitle.textColor = .charcoalGrey
                profileTypeImage.borderColor = .iceBlue
                profileTypeImage.borderWidth = 1.0
            }
        }
    }
    weak var delegate: ProfileTypeViewDelegate?
    
    @IBOutlet weak var profileTypeImage: UIImageView!
    @IBOutlet weak var profileTypeTitle: UILabel!
    @IBOutlet weak var profileTypeGesture: UITapGestureRecognizer! {
        didSet {
            profileTypeGesture.addTarget(self, action: #selector(self.didSelectProfile(_:)))
        }
    }
    
    @objc func didSelectProfile(_ sender: Any?) {
        if let del = delegate {
            del.didSelectProfile(profile: self)
        }
    }
    
}

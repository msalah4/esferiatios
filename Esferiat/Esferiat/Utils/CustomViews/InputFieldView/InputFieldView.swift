//
//  InputFieldView.swift
//  Coach You
//
//  Created by Mohammed Salah on 07/05/2020.
//  Copyright © 2020 msalah. All rights reserved.
//

import UIKit
import L10n_swift

enum InputType {
    case text
    case email
    case phone
    case number
    case birthDate
    case password
    case dropDown
//    case tag
    case tagWithDropdown
}


class InputFieldView: UIView {

    let nibName = "InputFieldView"
    var contentView: InputFieldViewContent?
    var didSelectDropDownClosure: (() -> Void)?
    var inputType: InputType = .text {
        didSet {
            updateInputType()
        }
    }
    
    @IBInspectable
    var titleText: String = ""
    
    @IBInspectable
    var placeHolderText: String = ""
    
    @IBInspectable
    var isSecret: Bool = false
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var inputImage: UIButton!
    @IBOutlet weak var tagsView: UIView!
    
//    var tagsField: WSTagsField!
    fileprivate let dropDownGesture = UITapGestureRecognizer()
    fileprivate var isShowingThePassword = false {
        didSet {
            contentTextField.isSecureTextEntry = isShowingThePassword
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
        contentView = view
        self.titleLabel = contentView!.titleLabel
        self.contentTextField = contentView!.contentTextField
        self.inputImage = contentView!.extraInfoImage
        self.errorLabel = contentView!.errorLabel
        self.tagsView = contentView!.tagsView
        
    }
    
   override func awakeFromNib() {
       super.awakeFromNib()
    self.titleLabel.text = titleText.l10n()
       self.contentTextField.placeholder = placeHolderText.l10n()
       self.inputImage.isHidden = !isSecret
       
   }

    func loadViewFromNib() -> InputFieldViewContent? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? InputFieldViewContent
    }
    
    func inputedValue() -> String {
        
//        if inputType != .tag {
            return contentTextField.text ?? ""
//        }
//        else {
//            return tagsField.text ?? ""
//        }
        
    }
    
    func updateInputType()  {
        switch inputType {
        case .text:
            break
        case .email:
            contentTextField.keyboardType = .emailAddress
            break
        case .phone:
            contentTextField.keyboardType = .phonePad
            break
        case .number:
            contentTextField.keyboardType = .numberPad
            break
        case .password:
            contentTextField.isSecureTextEntry = true
            inputImage.isHidden = false
            inputImage.setImage(UIImage(named: "eye"), for: .normal)
            inputImage.addTarget(self, action: #selector(self.didPressShowPassword(_:)), for: .touchUpInside)
            break
        case .birthDate:
            contentTextField.keyboardType = .numbersAndPunctuation
            break
//        case .tag:
//            setupInputAsTagView()
        case .dropDown:
            setupDropDown()
        case .tagWithDropdown :
            setupDropDown()
//            setupInputAsTagView()
            tagsView.isUserInteractionEnabled = false
//            setupInputAsTagView()
//            inputImage.setImage(UIImage(named: "dropDown"), for: .normal)
//            inputImage.isHidden = false
//            inputImage.addTarget(self, action: #selector(Self.didSelectDropDown(_:)), for: .touchUpInside)
        }
    }
    
    @objc func didPressShowPassword(_ sender: Any) {
        isShowingThePassword = !isShowingThePassword
        
        if !isShowingThePassword {
            inputImage.setImage(UIImage(named: "unselected_eye"), for: .normal)
        } else {
            inputImage.setImage(UIImage(named: "eye"), for: .normal)
        }
    }
    
    func setInputValue(_ txt: String) {
        
//        if inputType == .tag {
//            tagsField.text = txt
//        } else {
            contentTextField.text = txt
//        }
    }
    
    func setupDropDown() {
        inputImage.setImage(UIImage(named: "dropDown"), for: .normal)
        inputImage.isHidden = false
        dropDownGesture.addTarget(self, action: #selector(self.didSelectDropDown(_:)))
        contentTextField.addGestureRecognizer(dropDownGesture)
        
    }
    
    @objc func didSelectDropDown (_ sender: Any) {
        if let update = didSelectDropDownClosure {
            update()
        }
    }
    
//    func setupInputAsTagView() {
//        tagsField = WSTagsField()
//        tagsView.isHidden = false
//        tagsField.frame = tagsView.bounds
//        tagsView.addSubview(tagsField)
//
//        //tagsField.translatesAutoresizingMaskIntoConstraints = false
//        //tagsField.heightAnchor.constraint(equalToConstant: 150).isActive = true
//
//        (tagsField as UIView).cornerRadius = 15.0
//        tagsField.spaceBetweenLines = 10
//        tagsField.spaceBetweenTags = 5
//
//        //tagsField.numberOfLines = 3
//        //tagsField.maxHeight = 100.0
//
//        tagsField.layoutMargins = UIEdgeInsets(top: 2, left: 12, bottom: 2, right: 6)
//        tagsField.contentInset = UIEdgeInsets(top: 10, left: 12, bottom: 10, right: 10) //old padding
//
//        tagsField.placeholder = "Enter a Position"
//        tagsField.placeholderColor = .veryLightPink
//        tagsField.placeholderAlwaysVisible = false
//        tagsField.tintColor = .mutedPurple
//        tagsField.backgroundColor = .paleGrey
//        tagsField.textField.returnKeyType = .continue
//        tagsField.delimiter = ""
//
//        tagsField.textDelegate = self
//
//    }

}

extension InputFieldView: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //if textField == tagsField {
        //}
        return true
    }

}


class InputFieldViewContent: UIView {
    
    @IBOutlet weak var tagsView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var contentTextField: UITextField!
    @IBOutlet weak var extraInfoImage: UIButton!
}

//
//  AppDelegate.swift
//  Esferiat
//
//  Created by Mohammed Salah on 29/06/2020.
//  Copyright © 2020 MSalah. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        LocalizeService.shared.startService()
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.backgroundColor = UIColor.white
        self.window?.makeKeyAndVisible()
        let storybaord = UIStoryboard.init(name: "Login", bundle: nil)
        let baseView = storybaord.instantiateViewController(withIdentifier: "start")
        window?.rootViewController = baseView
//        UIApplication.shared.didChangeLang()
        return true
    }

    
}

